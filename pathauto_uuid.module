<?php

/**
 * @file pathauto_uuid.module
 */
 
 /**
  * Implements hook_menu
  */
function pathauto_uuid_menu() {
  $items['admin/config/search/path/uuid'] = array(
    'title' => 'UUID',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pathauto_uuid_admin_form'),
    'access arguments' => array('administer pathauto uuid'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 50,
  );
  
  return $items;
}

/**
 * Implements hook_permission().
 */
function pathauto_uuid_permission() {
  return array(
    'administer pathauto uuid' => array(
      'title' => t('Administer pathauto uuid'),
      'description' => t('Allows a user to configure the pathauto uuid module.'),
    ),
  );
}

/**
 * Form builder; Configure the node types to implement UUID on.
 */
function pathauto_uuid_admin_form($form, &$form_state) {
  $node_names = node_type_get_names();
  $node_names_default = explode(",", variable_get('pathauto_uuid_types', ","), -1);
  
  $form['header'] = array(
    '#markup' => t('Select which node types will have automatic UUIDs generated for them.'),
  );
  $form['node_types'] = array(
    '#type' => 'checkboxes', 
    '#title' => t('Content types'), 
    '#options' => $node_names,
    '#description' => t(''),
    '#default_value' => $node_names_default,
  );
  $form['submit'] = array('#type' => 'submit', '#value' => t('Submit'));
  
  return $form;
}

/**
 * Admin form submit function
 */
function pathauto_uuid_admin_form_submit($form, &$form_state) {
  $pathauto_uuid_types = "";
  foreach ($form_state['values']['node_types'] as $key => $value) {
    if ($value) {
      $pathauto_uuid_types .= $value .', ';
    }
  }
  variable_set('pathauto_uuid_types', $pathauto_uuid_types);
}

/**
 * Implement hook_node_insert
 */
function pathauto_uuid_node_insert($node) {
  if ($node->nid && $node->title) {
    module_load_include('inc', 'pathauto', 'pathauto');
    $record = array (
      "nid" => $node->nid,
      "title" => $node->title,
      "uuid" => pathauto_uuid_generate($node->title),
    );
    drupal_write_record ('pathauto_uuid', $record);
  }
}

/**
 * Implement hook_node_update
 */
function pathauto_uuid_node_update($node) {
  if ($node->nid && $node->title) {
    module_load_include('inc', 'pathauto', 'pathauto');
    $record = array (
      "nid" => $node->nid,
      "title" => $node->title,
      "uuid" => pathauto_uuid_generate($node->title),
    );
    drupal_write_record ('pathauto_uuid', $record, 'nid');
  }
}

/**
 * Implement hook_node_delete
 */
function pathauto_uuid_node_delete($node) {
  db_query("DELETE from {pathauto_uuid} WHERE nid=:nid", array(':nid' => $node->nid));
}

/**
 * Generate the UUID
 */
function pathauto_uuid_generate($node_title) {
  $uuid = $uuid_org = pathauto_cleanstring($node_title);
  // Check for uniqueness
  $result = db_query("SELECT uuid FROM {pathauto_uuid} WHERE uuid LIKE :uuid", array(':uuid' => $uuid."%"));
  $results = $result->fetchAll();
  foreach($results AS $key=>$record) {
    if ($uuid == $record->uuid) {
      $uuid = $uuid_org ."-". $key;
    }
    else {
      break;
    }
  }
  return $uuid;
}

/**
 * Return node object from UUID
 */
function pathauto_uuid_get_node($uuid) {
    $nid = db_query("SELECT nid FROM {pathauto_uuid} WHERE uuid = :uuid", array(':uuid' => $uuid))->fetchField();
    if ($nid) {
      return node_load($nid);
    }
    else {
      return FALSE;
    }
}

/**
 * Return UUID from node id
 */
function pathauto_uuid_get_uuid($nid) {
    $uuid = db_query("SELECT uuid FROM {pathauto_uuid} WHERE nid = :nid", array(':nid' => $nid))->fetchField();
    if ($uuid) {
      return $uuid;
    }
    else {
      return FALSE;
    }
}

